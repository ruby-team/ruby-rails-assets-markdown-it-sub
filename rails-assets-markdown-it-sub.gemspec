# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'rails-assets-markdown-it-sub/version'

Gem::Specification.new do |spec|
  spec.name          = "rails-assets-markdown-it-sub"
  spec.version       = RailsAssetsMarkdownItSub::VERSION
  spec.authors       = ["rails-assets.org"]
  spec.description   = "<sub> tag for markdown-it markdown parser."
  spec.summary       = "<sub> tag for markdown-it markdown parser."
  spec.homepage      = "https://github.com/markdown-it/markdown-it-sub"
  spec.license       = "MIT"

  spec.files         = `find ./* -type f | cut -b 3-`.split($/)
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "rake"
end
